In this Orange, California location, Dr. Pham-Nguyen has built a diversified practice where patients can receive nearly every dental treatment they will ever need, all under one roof. From the simplest filling to root canal therapy,  we provide a wide array of dental services to patients of all ages.

Address: 747 E Chapman Ave, Orange, CA 92866, USA

Phone: 714-532-4664
